import datetime
import sys

import fdb
import json

import psutil
from selenium import webdriver
from selenium.common import NoSuchElementException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions


def main(date_from, date_to):
    with open("settings.json", encoding="utf-8") as settings_file:
        settings = json.loads("".join(settings_file.readlines()))[0]

    if "chrome.exe" in (i.name() for i in psutil.process_iter()):
        print("Nejdříve zavřete Chrome")
        return

    connection = fdb.connect(
        host=settings["fb_host"], database=settings["fb_database"],
        user=settings["fb_user"], password=settings["fb_password"]
    )

    options = webdriver.ChromeOptions()

    options.add_argument(
        f"--user-data-dir=C:\\Users\\{settings['pc_user']}\\AppData\\Local\\Google\\Chrome\\User Data\\")

    driver = webdriver.Chrome(options=options)

    url = f"https://apps.indusuite.com/productivity/weld-sessions/sessions?startDate={date_from}&endDate={date_to}"

    driver.get(url)

    try:
        username_input = driver.find_element(By.ID, "username")
        username_input.send_keys(settings["user_name"])
        driver.find_element(By.ID, "login-next").click()

        password_input = WebDriverWait(driver, 60).until(
            expected_conditions.presence_of_element_located((By.ID, "password")))
        password_input.send_keys(settings["password"])

        driver.find_element(By.ID, "login").click()

        WebDriverWait(driver, 60).until(
            expected_conditions.presence_of_element_located((By.ID, "app-productivity")))

        driver.get(url)
    except NoSuchElementException:
        print("Uživatel je přihlášen")

    WebDriverWait(driver, 120).until(
        expected_conditions.presence_of_element_located((By.XPATH, "//td[contains(@class, 'text-start')]")))

    t_head = driver.find_element(By.TAG_NAME, "thead").get_attribute("innerHTML")

    trs_head = split_all_tags(t_head, "<span>", "</span>")

    """ pokud je stránka v češtině
    date_index = trs_head.index("Vytvořeno")
    arc_time_index = trs_head.index("Čas oblouku")
    current_index = trs_head.index("Prům. A")
    voltage_index = trs_head.index("Prům. V")
    power_index = trs_head.index("Prům. P")
    """

    date_index = trs_head.index("Created")
    arc_time_index = trs_head.index("Arc time")
    current_index = trs_head.index("Avg A")
    voltage_index = trs_head.index("Avg V")
    power_index = trs_head.index("Avg P")

    t_body = driver.find_element(By.TAG_NAME, "tbody").get_attribute("innerHTML")

    trs = split_all_tags(t_body, "<tr class=\"\">", "</tr>")

    sql_data = []

    min_arc_time = settings["min_arc_time"]

    for tr in trs:
        tds = split_all_tags(tr, "<td class=\"text-start\">", "</td>")

        date = split_all_tags(tds[date_index], "<span><!----><!----><!----><!----><!----><span>",
                              "</span><!----></span>")[0]

        arc_time = tds[arc_time_index]
        if "min" in arc_time:
            arc_time = arc_time.split("min ")
            arc_time = int(arc_time[0]) * 60 + int(arc_time[1].replace("s", ""))
        else:
            arc_time = int(arc_time.replace("s", ""))

        if arc_time < min_arc_time:
            continue

        """ pro datum v češtině
        date = date.strip().replace("ledna", "01").replace("února", "02").replace("března", "03") \
            .replace("dubna", "04").replace("května", "05").replace("června", "06") \
            .replace("července", "07").replace("srpna", "08").replace("září", "09") \
            .replace("října", "10").replace("listopadu", "11").replace("prosince", "12")
        date = int(datetime.datetime.strptime(date, "%d. %m %Y %H:%M:%S").timestamp())
        """

        date = date.strip().split(" ", 2)
        date = date[0] + " " + date[1].replace("st", "").replace("nd", "").replace("rd", "").replace("th", "") + date[2]
        date = int(datetime.datetime.strptime(date, "%B %d,%Y %I:%M:%S %p").timestamp())

        current = tds[current_index].replace("A", "").strip()
        voltage = tds[voltage_index].replace("V", "").strip()
        power = tds[power_index].replace("kW", "").strip()

        """ výpis hodnot
        print(date)
        print(arc_time)
        print(current)
        print(voltage)
        print(power)
        """

        sql_data.append((date, arc_time, current, voltage, power))

    driver.quit()

    print("Odesílání dat do databáze")

    cursor = connection.cursor()

    cursor.executemany(
        "INSERT INTO WELD_CLOUD_DATA (VYTVORENO, CAS_OBLOUKU, PRUM_A, PRUM_V, PRUM_P) VALUES (?, ?, ?, ?, ?)",
        sql_data
    )

    connection.commit()

    print("Hotovo")


def split_all_tags(string, tag_name_start, tag_name_end):
    split = string.split(tag_name_start)
    tag_list = []
    for i in range(1, len(split)):
        tag_list.append(split[i].split(tag_name_end, 1)[0])

    return tag_list


if __name__ == '__main__':
    if len(sys.argv) != 3:
        print("Program přijímá dva parametry: začátek a konec weld sessions")
    else:
        main(sys.argv[1], sys.argv[2])
